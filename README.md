check\_xmpp
===========

This plugin uses Perl Net::XMPP to connect to an XMPP server, to test whether
it is accepting connections. It does not perform any authentication.

Installation
------------

    $ perl Makefile.PL
    $ make
    $ sudo make install

Usage
-----

    Usage: check_xmpp [--hostname|-H HOSTNAME] [--port|-p PORT] [--tls|-s]
    
     -?, --usage
       Print usage information
     -h, --help
       Print detailed help screen
     -V, --version
       Print version information
     --extra-opts=[section][@file]
       Read options from an ini file. See https://www.monitoring-plugins.org/doc/extra-opts.html
       for usage and examples.
     -H, --hostname=HOSTNAME
       Hostname or IP address of device to check
     -p, --port=PORT
       TCP port to check
     -s, --tls
       Use a secure connection
     -t, --timeout=INTEGER
       Seconds before plugin times out (default: 15)
     -v, --verbose
       Show details for command-line debugging (can repeat up to 3 times)



Thanks
------

This was written on company time with my employer [Inspire Net][1], who has
generously allowed me to open source it.

License
-------

Copyright (c) [Tom Ryder][2]. Distributed under [MIT License][3].

[1]: https://www.inspire.net.nz/
[2]: https://sanctum.geek.nz/
[3]: https://opensource.org/licenses/MIT
